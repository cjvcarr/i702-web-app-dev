	<!DOCTYPE html>
<?php
include("../config/config.php");
include("../includes/header.php");
$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
if ($conn->connect_error)
  die("Connection to database failed:" .
    $conn->connect_error);
$conn->query("set names utf8");
 
$statement = $conn->prepare(
"INSERT INTO `shop_user` (
    `email`,
    `password`,
    `first_name`,
    `last_name`,
    `phone`,
    `dob`,
    `salutation`,
    `vatin`,
    `company`,
    `country`,
    `address`)
VALUES (?, PASSWORD(?), ?, ?, ?, ?, ?, ?, ?, ?, ?)");
 
# whenever you get "call to a member function ... on a non-object" this means something
# is failing **before** that line so you have to manually check for errors like this:
if (!$statement) die("Prepare failed: (" . $conn->errno . ") " . $conn->error);
 
$statement->bind_param("sssssssssss",
    $_POST["email"],
    $_POST["password"],
    $_POST["first_name"],
    $_POST["last_name"],
    $_POST["phone"],
    $_POST["dob"],
    $_POST["salutation"],
    $_POST["vatin"],
    $_POST["company"],
    $_POST["country"],
    $_POST["address"]);
 
if ($statement->execute()) {
    $_SESSION["registered"] = "regsuccess";
		header("Location: ../index.php");
} else {
    if ($statement->errno == 1062) {
      $_SESSION["registered"] = "regfailed1062";
			header("Location: register.php");
    } else {
       // This will result in 500 Internal server error
      $_SESSION["registered"] = "regfailed";
			header("Location: register.php");
    }
}
?>
