<!DOCTYPE html>
<html>
	<?php include('../includes/header.php'); 
				include('../config/conn.php');
				include('../includes/navbar-pages.php'); 
	$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
	if ($conn->connect_error)
	  die("Connection to database failed:" .
	    $conn->connect_error);
	$statement = $conn->prepare('SELECT `product_id`, `count`, `unit_price` FROM `shop_order_item` WHERE `order_id` = ?');
	$statement->bind_param("i", $_GET["id"]);
	$statement->execute();
	$results = $statement->get_result();
	?>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Introduction to this guy's website">
    <title>This goes into the titlebar</title>
    <link rel="css/style.css" type="text/css"/>
    <meta name="viewport" content="width=device-width"/><!-- Disable zoom on smartphone -->
  </head>
  <body style="padding-top: 50px;">
		<?php checkLoginSuccess(); ?>
		<div class="jumbotron" style="margin-bottom:0;background:#83b819;">
			<div class="container" >
	  	  <h1>Andy's Autos</h1> 
 			  <p>Your one stop shop for every manufacturer on the market!</p>
			</div>
		</div>
		<div class="container"><h1>Products</h1>
		<div class="table-responsive">
<table class="table table-striped table-hover">
<thead>
  <tr>
    <th>Image</th>
		<th>Product name</th>
		<th>Unit Price</th>
    <th>QTY</th> 
		<th>Total Price</th>
  </tr>
</thead>
<tbody>
<?php
$grand_total = 0;
while ($row = $results->fetch_assoc()) {
$statement_product = $conn->prepare('SELECT `name`, `image` FROM `shop_products` WHERE `id` = ?');
$statement_product->bind_param("i", $row['product_id']);
$statement_product->execute();
$product_res = $statement_product->get_result();
$products = $product_res->fetch_assoc();
$total = floatval($row['count'])*floatval($row['unit_price']);
?>
  <tr>
		<td><img class="img-thumbnail" src="<?=$products['image'];?>" alt="<?=$row["name"]?>"width="130" height="100"></td>
    <td><h4><?=$products['name'];?></h4></td>
		<td><h4>€<?=money_format('%i', $row['unit_price']);?></h4></td>
    <td><h4><?=$row['count'];?></h4></td> 
		<td><h4>€<?=money_format('%i', $total);?></h4></td>
  </tr>
<?php 
  $grand_total = $grand_total + $total;
} ?>
</tbody>
<tfoot>
  <tr>
	<td><h3>Grand total:</h3></td>
	<td></td>
	<td></td>
	<td></td>
	<td><h3>€<?=money_format('%i', $grand_total);?></h3></td>
</tfoot>
</table>
</div>		

  </body>
</html>

<?php
		function checkLoginSuccess() {
		if ($_SESSION["logged_in"] == 'loginsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Login successful!</strong> Indicates a successful or positive action.
			</div>
			<?php } 
		if ($_SESSION["logged_in"] == 'loginfailed') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-danger alert-dismissable fade in" style="margin-bottom:0;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			<strong>Login failed!</strong> Indicates a failed or dangerous action.
			</div>
			<?php }
		if ($_SESSION["logged_in"] == 'logoutsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Logged out. </strong> Indicates a successful or positive action.
			</div>
			<?php } 
		} ?>
<!-- Page specific stuff goes here -->
