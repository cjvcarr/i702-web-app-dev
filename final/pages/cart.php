<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Introduction to this guy's website">
    <title>Andy's Autos cart</title>
    <link rel="css/style.css" type="text/css"/>
    <meta name="viewport" content="width=device-width"/><!-- Disable zoom on smartphone -->
  </head>
<?php 
	include('../includes/header.php'); 
	include('../config/conn.php');
	include('../includes/navbar-pages.php'); 
	$product_id = intval($_POST["id"]);
	if (array_key_exists($product_id, $_SESSION["cart"])) {
		if ($product_id > 0) { $_SESSION["cart"][$product_id] += intval($_POST["quantity"]); }
		} else { if ($product_id > 0) {$_SESSION["cart"][$product_id] = intval($_POST["quantity"]); }
	}
	$id = implode(",", array_keys($_SESSION["cart"]));
	$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
	$results = $conn->query("SELECT name, price, id, image FROM `shop_products` WHERE id IN ($id);");
	$grand_total = 0;
?>
	
  <body style="padding-top: 50px; padding-bottom:50px">
		<?php checkLoginSuccess(); ?>
		<div class="jumbotron" style="margin-bottom:0;background:#83b819;">
			<div class="container" >
	  	  <h1>Andy's Autos</h1> 
 			  <p>Your one stop shop for every manufacturer on the market!</p>
			</div>
		</div>
<?php if (empty($_SESSION["cart"])==false) { ?>
<div class="container"><h1>Shopping Cart</h1>
<form id="form_test" action="ordersubmit.php" method="post">
	<table id="cartitems" class="table table-striped table-hover">
	<thead>
		<tr>
	    <th>Image</th>
			<th>Product name</th>
			<th>Unit Price</th>
  	  <th>QTY</th> 
			<th>Total Price</th>
  	</tr>
	</thead>
<?php
	while ($row = $results->fetch_assoc()) {
		$total = floatval($_SESSION["cart"][$row['id']])*floatval($row['price']);    
?>
			<tr>
				<td><img class="img-thumbnail" src="<?=$row["image"];?>" alt="<?=$row["name"]?>"width="130" height="100"></td>
				<td><h4><a href="description.php?id=<?=$row['id']?>"><?=$row["name"]?></a></h4></td>
				<td><h4>€<?=money_format('%i', $row["price"])?></h4></td>
 				<td><h4><?=$_SESSION["cart"][$row['id']]?></h4></td> 
				<td><h4>€<?=money_format('%i', $total);?></h4></td>
				<input type="hidden" name="id[]" value=<?=$row['id']?>>
				<input type="hidden" name="quantity[]" value=<?=$_SESSION["cart"][$row["id"]]?>>
				<input type="hidden" name="price[]" value=<?=$row["price"]?>>
			</tr>
    <?php
      $grand_total = $grand_total + (floatval($row["price"])*floatval($_SESSION["cart"][$row['id']]));
  }
    $conn->close();?>

</tbody>
<tfoot>
  <tr>
		<td><h3>Grand total:</h3></td>
		<td></td>
		<td></td>
		<td></td>
		<td><h3><?=$grand_total.'€';?></h3></td>
	</tr>
</tfoot>
</table>
<?php if (empty($_SESSION["user"] == false)) { ?>
<button type="submit" class="btn btn-primary btn-block"">Place order</button>
<?php } ?>
</form>
</div>
	
<?php } else { ?>
<div class="container"><h1>Your shopping cart is empty!</h1></div> <?php } ?>
</body>
</html>

<?php
		function checkLoginSuccess() {
		if ($_SESSION["logged_in"] == 'loginsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Login successful!</strong> Indicates a successful or positive action.
			</div>
			<?php } 
		if ($_SESSION["logged_in"] == 'loginfailed') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-danger alert-dismissable fade in" style="margin-bottom:0;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			<strong>Login failed!</strong> Indicates a failed or dangerous action.
			</div>
			<?php }
		if ($_SESSION["logged_in"] == 'logoutsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Logged out. </strong> Indicates a successful or positive action.
			</div>
			<?php } 
		} ?>

<!-- Page specific stuff goes here -->
