<!DOCTYPE html>
<html>
	<?php include('../includes/header.php'); 
				include('../config/conn.php');
				include('../includes/navbar-pages.php'); 
	?>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Introduction to this guy's website">
    <title>Andy's Autos products</title>
    <link rel="css/style.css" type="text/css"/>
    <meta name="viewport" content="width=device-width"/><!-- Disable zoom on smartphone -->
  </head>
  <body style="padding-top: 50px;">
		<?php checkLoginSuccess(); ?>
		<div class="jumbotron" style="margin-bottom:0;background:#83b819;">
			<div class="container" >
	  	  <h1>Andy's Autos</h1> 
 			  <p>Your one stop shop for every manufacturer on the market!</p>
			</div>
		</div>
		<div class="container"><h1>Products</h1>
			<?php include('../includes/databasecall.php');?>	
		</div>
  </body>
</html>

<?php
		function checkLoginSuccess() {
		if ($_SESSION["logged_in"] == 'loginsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Login successful!</strong> Indicates a successful or positive action.
			</div>
			<?php } 
		if ($_SESSION["logged_in"] == 'loginfailed') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-danger alert-dismissable fade in" style="margin-bottom:0;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			<strong>Login failed!</strong> Indicates a failed or dangerous action.
			</div>
			<?php }
		if ($_SESSION["logged_in"] == 'logoutsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Logged out. </strong> Indicates a successful or positive action.
			</div>
			<?php } 
		} ?>
<!-- Page specific stuff goes here -->
<?php include "../includes/footer.php" ?>
