<!DOCTYPE html>
<html>
	<?php include('../includes/header.php'); 
				include('../config/conn.php');
				include('../includes/navbar-pages.php'); 
if (empty($_POST) == false) {
$statement = $conn->prepare(
"INSERT INTO `shop_order` (
    `user_id`)
VALUES (?)");
$statement->bind_param("s", $_SESSION["user"]);
$statement->execute();	

$statement2 = $conn->prepare("SELECT id FROM shop_order WHERE user_id = ? ORDER BY id DESC LIMIT 1");
$statement2->bind_param("i", $_SESSION["user"]);
$statement2->execute();
$results2 = $statement2->get_result();
$orderid = $results2->fetch_assoc();


?>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Introduction to this guy's website">
    <title>This goes into the titlebar</title>
    <link rel="css/style.css" type="text/css"/>
    <meta name="viewport" content="width=device-width"/><!-- Disable zoom on smartphone -->
  </head>
  <body style="padding-top: 50px;">
		<div class="jumbotron" style="margin-bottom:0;background:#83b819;">
			<div class="container" >
	  	  <h1>Andy's Autos</h1> 
 			  <p>Your one stop shop for every manufacturer on the market!</p>
			</div>
		</div>
		<div class="container"><h1>Order submit successful</h1>
<?php			
$i = 0;
	foreach($_POST["id"] as $value) {
$statement3 = $conn->prepare(
"INSERT INTO `shop_order_item` (`order_id`, `product_id`, `count`, `unit_price`) VALUES (?, ?, ?, ?)");
$statement3->bind_param("iiis", $orderid["id"],$_POST["id"][$i],$_POST["quantity"][$i],$_POST["price"][$i]);
$statement3->execute();
$i = $i + 1;
	} 
} else { $previous = strtok($_SERVER["HTTP_REFERER"],'?');
	header("Location: ../index.php");
	} ?>
		</div>
  </body>
</html>

<!-- Page specific stuff goes here -->
<?php include "../includes/footer.php" ?>
