<!DOCTYPE html>
<html>
	<?php include('../includes/header.php'); 
				include('../config/conn.php');
				include('../includes/navbar-pages.php'); 
$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
if ($conn->connect_error)
  die("Connection to database failed:" .
    $conn->connect_error);
$statement = $conn->prepare(
  "SELECT `name`, `description`, `price`, `image` FROM" .
  " `shop_products` WHERE `id` = ?");
$statement->bind_param("i", $_GET["id"]);
$statement->execute();
$results = $statement->get_result();
$row = $results->fetch_assoc();
?>
	
<head>
  <meta charset="utf-8"/>
  <meta name="description" content="Introduction to this guy's website">
    <title>Andy's Autos description</title>
  <link rel="css/style.css" type="text/css"/>
  <meta name="viewport" content="width=device-width"/><!-- Disable zoom on smartphone -->
</head>

<body style="padding-top: 50px;">
	<?php checkLoginSuccess(); ?>
	<div class="jumbotron" style="margin-bottom:0;background:#83b819;">
		<div class="container">
	 	  <h1>Andy's Autos</h1> 
 			  <p>Your one stop shop for every manufacturer on the market!</p>
		</div>
	</div>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<a href="<?=$row['image']?>">
				<img class="img-responsive" src="<?=$row["image"];?>" alt="<?=$row["name"]?>" style="width:100%">
			</a>
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-6">
			<h1><?=$row["name"];?></h1>
			<h4><?=$row["description"];?></h4>
			<h2>€<?=$row["price"];?></h2>
	<form method="post" action="cart.php">
		<div class="form-group">
  	<label for="selection">Quantity:</label>
  	<select name="quantity" class="form-control" style="width:200px">
    	<option>1</option>
    	<option>2</option>
    	<option>3</option>
    	<option>4</option>
  	</select>
		</div>
  <input type="hidden" name="id" value="<?=$_GET["id"];?>"/>
  <button type="submit" class="btn btn-primary"">Add to cart</button>
	</form>
	</div>
<?php include "../includes/footer.php" ?>
  </body>
</html>

<?php
		function checkLoginSuccess() {
		if ($_SESSION["logged_in"] == 'loginsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Login successful!</strong> Indicates a successful or positive action.
			</div>
			<?php } 
		if ($_SESSION["logged_in"] == 'loginfailed') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-danger alert-dismissable fade in" style="margin-bottom:0;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			<strong>Login failed!</strong> Indicates a failed or dangerous action.
			</div>
			<?php }
		if ($_SESSION["logged_in"] == 'logoutsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Logged out. </strong> Indicates a successful or positive action.
			</div>
			<?php } 
		} ?>
<!-- Page specific stuff goes here -->

