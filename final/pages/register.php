<!DOCTYPE html>
<html>
	<?php include('../includes/header.php'); 
				include('../config/conn.php');
				include('../includes/navbar-pages.php'); 
	?>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Introduction to this guy's website">
    <title>Andy's Autos registration</title>
    <link rel="css/style.css" type="text/css"/>
    <meta name="viewport" content="width=device-width"/><!-- Disable zoom on smartphone -->
  </head>
  <body style="padding-top: 50px;">
		<?php checkLoginSuccess(); ?>
		<?php checkRegisterSuccess(); ?>
		<div class="container"><h1>Registration page</h1>
	<form class="form-horizontal" method="post" action="regsubmit.php">
  <div class="form-group">
    <label class="control-label col-sm-2" for="email">E-mail</label>
		<div class="col-sm-10">
    <input type="email" class="form-control" name="email" required/>
  	</div>
	</div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="password">Password</label>
		<div class="col-sm-10">
    <input type="password" class="form-control" name="password" required/>
  	</div>
	</div>
  <div class="form-group">
		<label class="control-label col-sm-2">Country</label>
		<div class="col-sm-10">
    <select name="country" class="form-control">
      <option value="ee">Estonia</option>
      <option value="lt">Latvia</option>
      <option value="lv">Lithuania</option>
    </select>
  	</div>
	</div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="phone">Telephone number</label>
		<div class="col-sm-10">
    <input type="tel" class="form-control"/>
	  </div>
	</div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="vatin">VAT indication number</label>
		<div class="col-sm-10">
    <input type="text" pattern="([A-Z0-9]{4,14})?$" class="form-control"/>
	  </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="dob">Date of birth</label>
		<div class="col-sm-10">
    <input type="date" name="dob" placeholder="dd/mm/yyyy" required class="form-control"/>
	  </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="first_name">First name</label>
		<div class="col-sm-10">
    <input type="text" name="first_name" required class="form-control"/>
		</div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="last_name">Last name</label>
		<div class="col-sm-10">
    <input type="text" name="last_name" required class="form-control"/>
	  </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
</form>
		</div>
  </body>
</html>

<?php
		function checkLoginSuccess() {
		if ($_SESSION["logged_in"] == 'loginsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Login successful!</strong> Indicates a successful or positive action.
			</div>
			<?php } 
		if ($_SESSION["logged_in"] == 'loginfailed') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-danger alert-dismissable fade in" style="margin-bottom:0;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			<strong>Login failed!</strong> Indicates a failed or dangerous action.
			</div>
			<?php }
		if ($_SESSION["logged_in"] == 'logoutsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Logged out. </strong> Indicates a successful or positive action.
			</div>
			<?php } 
		} ?>

<?php
		function checkRegisterSuccess() {
		if ($_SESSION["registered"] == 'regfailed1062') { 
			$_SESSION["registered"] = NULL; ?>
			<div class="alert alert-danger alert-dismissable fade in" style="margin-bottom:0;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			<strong>Registration failed!</strong> That e-mail is already registered! Try again with a different address.
			</div>
			<?php } 
		if ($_SESSION["registered"] == 'regfailed') { 
			$_SESSION["registered"] = NULL; ?>
			<div class="alert alert-danger alert-dismissable fade in" style="margin-bottom:0;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			<strong>Registration failed!</strong> Registration failed! Please try again later.
			</div>
			<?php } 
		} ?>
<!-- Page specific stuff goes here -->
