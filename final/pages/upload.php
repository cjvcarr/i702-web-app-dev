<!DOCTYPE html>
<html>
	<?php include('../includes/header.php'); 
				include('../config/conn.php');
				include('../includes/navbar-pages.php'); 
	?>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Andy's Autos">
    <title>Andy's Autos upload</title>
    <link rel="css/style.css" type="text/css"/>
    <meta name="viewport" content="width=device-width"/><!-- Disable zoom on smartphone -->
  </head>
  <body style="padding-top: 50px;">
		<?php checkLoginSuccess(); ?>
		<div class="jumbotron" style="margin-bottom:0;background:#83b819;">
			<div class="container" >
	  	  <h1>Andy's Autos</h1> 
 			  <p>Your one stop shop for every manufacturer on the market!</p>
			</div>
		</div>
		<?php if ($_SESSION["user"] == '1') { ?>
		<div class="container"><h1>Upload new product</h1>
			<form class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label class="control-label col-sm-2" for="product_title">Product name:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="product_title" required/>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="product_description">Product description:</label>
					<div class="col-sm-10">
						<textarea class="form-control" rows="5" name="product_description" required></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="product_price">Product price:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="product_price" required/>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="product_image">Product image:</label>
					<div class="col-sm-10">
						<input type="file" class="form-control" name="product_image" required/>
					</div>
				</div>
				<div class="form-group"> 
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default">Submit</button>
			    </div>
			  </div>
			</form>
		</div>
		<?php
    if (array_key_exists("product_image", $_FILES)) {
      $finfo = finfo_open(FILEINFO_MIME_TYPE);
      $mimetype = finfo_file($finfo, $_FILES["product_image"]["tmp_name"]);
      if ($mimetype != "image/jpeg" && $mimetype != "image/png") die("Go away!");
 
      $checksum = sha1(file_get_contents(
        $_FILES["product_image"]["tmp_name"])) . "." .
        pathinfo($_FILES["product_image"]["name"], PATHINFO_EXTENSION);
      if (!file_exists("uploads/" . $checksum)) {
        copy(
          $_FILES["product_image"]["tmp_name"],
          "../uploads/" . $checksum);
      }
$conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
if ($conn->connect_error)
die("Connection to database failed:" .
$conn->connect_error);
$conn->query("set names utf8");
 
$statement = $conn->prepare(
"INSERT INTO `shop_products` (`name`, `description`, `price`, `image`) VALUES (?, ?, ?, ?)");
$path = "../uploads/".$checksum;
if (!$statement) die("Prepare failed: (" . $conn->errno . ") " . $conn->error);
$statement->bind_param("ssss",
    $_POST["product_title"],
    $_POST["product_description"],
		$_POST["product_price"],
    $path);

$statement->execute();
	} 
} else { ?>
<div class="container"><h1>You must be an admin to add new products!</h1></div>
<?php } ?>
  </body>
</html>

<?php
		function checkLoginSuccess() {
		if ($_SESSION["logged_in"] == 'loginsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Login successful!</strong> Indicates a successful or positive action.
			</div>
			<?php } 
		if ($_SESSION["logged_in"] == 'loginfailed') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-danger alert-dismissable fade in" style="margin-bottom:0;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			<strong>Login failed!</strong> Indicates a failed or dangerous action.
			</div>
			<?php }
		if ($_SESSION["logged_in"] == 'logoutsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Logged out. </strong> Indicates a successful or positive action.
			</div>
			<?php } 
		} ?>
<!-- Page specific stuff goes here -->

