<!DOCTYPE html>
<html>
	<?php include('includes/header.php'); 
				include('config/conn.php');
				include('includes/navbar-root.php'); 
	?>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Introduction to this guy's website">
    <title>Andy's Autos</title>
    <link rel="css/style.css" type="text/css"/>
    <meta name="viewport" content="width=device-width"/><!-- Disable zoom on smartphone -->
  </head>
  <body style="padding-top: 50px;">
		<?php checkLoginSuccess(); ?>
		<?php checkRegisterSuccess(); ?>
		<div class="jumbotron" style="margin-bottom:0;background:#83b819;">
			<div class="container" >
	  	  <h1>Andy's Autos</h1> 
 			  <p>Your one stop shop for every manufacturer on the market!</p>
			</div>
		</div>
	<div class="container">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="uploads/fb8bb903866df58557250d5653be0bf8450081cf.jpg" alt="Ford Mustang">
    </div>
    <div class="item">
      <img src="uploads/eb4ac8930de56b040a920c51a53f2dcc76b3a473.jpg" alt="Nissan Qashqai">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
  </body>
</html>

<?php
		function checkLoginSuccess() {
		if ($_SESSION["logged_in"] == 'loginsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Login successful!</strong> You can now continue browsing the shop.
			</div>
			<?php } 
		if ($_SESSION["logged_in"] == 'loginfailed') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-danger alert-dismissable fade in" style="margin-bottom:0;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			<strong>Login failed!</strong> Try again or register as a new user!.
			</div>
			<?php }
		if ($_SESSION["logged_in"] == 'logoutsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Logged out. </strong> Come back soon!
			</div>
			<?php } 
		} ?>

<?php
		function checkRegisterSuccess() {
			if ($_SESSION["registered"] == 'regsuccess') { 
			$_SESSION["registered"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Registration successful!</strong> Please log in to use your account.
			</div>
			<?php } 
		} ?>
<!-- Page specific stuff goes here -->
<? include "footer.php" ?>
