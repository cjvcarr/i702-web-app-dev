<html>
<head>
<script>
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>
</head>
<nav onload="startTime()" class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="../index.php">Andy's Autos</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="../index.php">Home</a></li>
        <li><a href="products.php">Products	</a></li>
        <li><a href="orders.php">Orders</a></li> 
				<?php if ($_SESSION["user"] == '1') { ?>
        <li><a href="upload.php">Add new product</a></li> <?php } ?>
      </ul>
      <ul class="nav navbar-nav navbar-right">
				<li><a href="cart.php"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
			<?php 
				if (array_key_exists("user", $_SESSION)) { ?>
					<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>			
			<?php
			} else { ?>	
					<li><a href="register.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
	        <li></span><a href="#" data-toggle="modal" data-target="#loginformmodal"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
			<?php
			} ?>
        
      </ul>
    </div>
  </div>
</nav>
<div class="modal fade" id="loginformmodal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body">
           <form method="post" action="login.php">
  					<div class="form-group">
					    <label for="user">Username:</label>
					    <input type="text" class="form-control" name="user">
					  </div>
					  <div class="form-group">
					    <label for="password">Password:</label>
					    <input type="password" class="form-control" name="password">
					  </div>
					  	<input type="submit" class="btn btn-info" value="Login">
						</form> 
        </div>
      </div>
    </div>
  </div>
</html>
