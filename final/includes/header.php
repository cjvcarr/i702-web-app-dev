<html>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<?php
session_start();
if (!array_key_exists("cart", $_SESSION)) {
    $_SESSION["cart"] = array();
}
if (!array_key_exists("logged_in", $_SESSION)) {
		$_SESSION["logged_in"] = NULL;
}
if (!array_key_exists("registered", $_SESSION)) {
		$_SESSION["registered"] = NULL;
}
#ini_set('display_startup_errors', 1);
#ini_set('display_errors', 1);
#error_reporting(-1);
?>
