<?php
    $conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
    $results = $conn->query(
      "SELECT id,name,price,image FROM shop_products;");
 ?><div class="row"><?php
    while ($row = $results->fetch_assoc()) { ?>
			<div class="col-md-4">
				<div class="thumbnail">
     			<a href="description.php?id=<?=$row['id']?>">
       			<img class="img-responsive" src="<?=$row["image"];?>" alt="<?=$row["name"]?>" style="width:100%">
       				<div class="caption">
         				<h2><?=$row["name"]?></h2>
         				<h4>€<?=$row["price"]?></h4>
       				</div>
     			</a>
   			</div>
			</div>					
					
      <?php
    }
?> </div> <?php
 
    $conn->close();
 
  ?>
