<a href="index.php">Back to product listing</a>
 
<?php
$conn = new mysqli("localhost", "root", "toor", "webshop");
if ($conn->connect_error)
  die("Connection to database failed:" .
    $conn->connect_error);
$statement = $conn->prepare(
  "SELECT `name`, `description`, `price` FROM" .
  " `shop_products` WHERE `id` = ?");
$statement->bind_param("i", $_GET["id"]);
$statement->execute();
$results = $statement->get_result();
$row = $results->fetch_assoc();
?>
 
<span style="float:right;"><?=$row["price"];?>EUR</span>
<h1><?=$row["name"];?></h1>

<p>
  <?=$row["description"];?>
</p>


<form method="post" action="cart.php">
<select name="quantity">
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
</select>
  <input type="hidden" name="id" value="<?=$_GET["id"];?>"/>
  <input type="submit" value="Add to cart"/>
</form>
