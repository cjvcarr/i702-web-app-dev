<a href="orders.php">Back to order list</a>
 
<?php
$conn = new mysqli("localhost", "root", "toor", "webshop");
if ($conn->connect_error)
  die("Connection to database failed:" .
    $conn->connect_error);
$statement = $conn->prepare('SELECT `product_id`, `count`, `unit_price` FROM `shop_order_item` WHERE `order_id` = ?');
$statement->bind_param("i", $_GET["id"]);
$statement->execute();
$results = $statement->get_result();
?>
<table style="width:10%">
<tr>
    <th>Product name</th>
	<th>Unit Price</th>
    <th>QTY</th> 
	<th>Total Price</th>
</tr>
<?php
while ($row = $results->fetch_assoc()) {
$statement_product = $conn->prepare('SELECT `name` FROM `shop_products` WHERE `id` = ?');
$statement_product->bind_param("i", $row['product_id']);
$statement_product->execute();
$product_res = $statement_product->get_result();
$products = $product_res->fetch_assoc();

?>
  <tr>
    <td><?=$products['name'];?></td>
	<td><?=$row['unit_price'].€;?></td>
    <td><?=$row['count'];?></td> 
	<td><?=$row['count']*$row['unit_price'].€;?></td>
  </tr>
<?php } ?>
</table>
