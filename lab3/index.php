	<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Introduction to this guy's website">
    <title>This goes into the titlebar</title>
    <link rel="css/style.css" type="text/css"/>
    <script type="text/javascript" src="js/main.js"></script>
    <meta name="viewport" content="width=device-width, user-scalable=no"/><!-- Disable zoom on smartphone -->
  </head>
  <body>
  <?php include('databasecall.php');?>
    <header>
      <?php include ('header.php');?>
    </header>
    <nav>
      Navigation links go here
    </nav>
    <section>
      Product items go here
    </section>
    <article>
      The actual content goes here
      <?php echo "This, is hellõu from PHP!"; ?>
    </article>
    <aside>
      Context specific links go here
    </aside>
    <footer>
      Footer goes here
    </footer>
  </body>
 
</html>
<?php
require_once "config.php";
$conn = new mysqli(localhost, "root", "toor", "webshop");
if ($conn->connect_error)
  die("Connection to database failed:" .
    $conn->connect_error);
$conn->query("set names utf8"); // Support umlaut characters
?>
<!-- Page specific stuff goes here -->
<? include "footer.php" ?>
