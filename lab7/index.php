	<!DOCTYPE html>
<html>
	<?php include ('header.php');?>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Introduction to this guy's website">
    <title>This goes into the titlebar</title>
    <link rel="css/style.css" type="text/css"/>
    <script type="text/javascript" src="js/main.js"></script>
    <meta name="viewport" content="width=device-width, user-scalable=no"/><!-- Disable zoom on smartphone -->
  </head>
  <body>
    <header>

    </header>
    <nav>
		<?php 
				if (array_key_exists("user", $_SESSION)) { ?>
				<div class="row"> 
				  <div class="col-sm-1"><?php echo "Hello " . $_SESSION["user"]; ?></div>
						<form action="logout.php">
    				<div class="col-sm-1"><input type="submit" value="Log out" /></div>
				</form>  				
			<?php
			} else { ?>	
				<div class="row">
					<form method="post" action="login.php">
  					<div class="col-sm-2"><input type="text" name="user"/></div>		
						<div class="col-sm-2"><input type="password" name="password"/></div>	
						<div class="col-sm-2"><input type="submit" value="Log in!"/></div>
	  			</form>
				</div> 
			<?php
			} ?>
    </nav>
    <section>
      <?php include('databasecall.php');?>
    </section>
    <article>
      The actual content goes here
    </article>
    <aside>
      Context specific links go here
    </aside>
    <footer>
      Footer items go here
	  <a href="register.php">New user? Register here</a>
    </footer>
  </body>
 
</html>
<?php
require_once "config.php";
$conn = new mysqli("localhost", "root", "toor", "webshop");
if ($conn->connect_error)
  die("Connection to database failed:" .
    $conn->connect_error);
$conn->query("set names utf8"); // Support umlaut characters
?>
<!-- Page specific stuff goes here -->
<? include "footer.php" ?>
