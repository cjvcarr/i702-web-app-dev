<!DOCTYPE html>
<html>
	<?php include('header.php'); 
				include('config.php');
				include('navbar.php'); 
	?>
  <head>
    <meta charset="utf-8"/>
    <meta name="description" content="Introduction to this guy's website">
    <title>This goes into the titlebar</title>
    <link rel="css/style.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, user-scalable=no"/><!-- Disable zoom on smartphone -->
  </head>
  <body style="padding-top: 50px;">
		<?php checkLoginSuccess(); ?>
		<div class="jumbotron" style="margin-bottom:0;">
			<div class="container" >
	  	  <h1>Bootstrap Tutorial</h1> 
 			  <p>Bootstrap is the most popular HTML, CSS, and JS framework for developing
		 	  responsive, mobile-first projects on the web.</p> 
			</div>
		</div>

		<div class="container">
	    <div class="container fluid">
  	    <h1>Your shop name goes here </h1>
  	  </div>
  	  <nav>
  	    Navigation
  	  </nav>
  	  <section>
  	    Product items go here
  	  </section>
  	  <article>
  	   Articles here
  	  </article>
  	  <aside>
  	    Context specific links go here
  	  </aside>
  	  <footer>
  	    Footer goes here
  	  </footer>
		</div>
  </body>
</html>

<?php
		function checkLoginSuccess() {
		if ($_SESSION["logged_in"] == 'loginsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Login successful!</strong> Indicates a successful or positive action.
			</div>
			<?php } 
		if ($_SESSION["logged_in"] == 'loginfailed') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-danger alert-dismissable fade in" style="margin-bottom:0;">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  			<strong>Login failed!</strong> Indicates a failed or dangerous action.
			</div>
			<?php }
		if ($_SESSION["logged_in"] == 'logoutsuccess') { 
			$_SESSION["logged_in"] = NULL; ?>
			<div class="alert alert-success alert-dismissable fade in" style="margin-bottom:0;">
  			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>Logged out. </strong> Indicates a successful or positive action.
			</div>
			<?php } 
		} ?>
