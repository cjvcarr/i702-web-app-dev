		<!DOCTYPE html>
<?php
// This is login.php, here we check if user provided proper credentials
#var_dump($_POST); // This is just to check that the data gets to server
include "config.php";
include "header.php";

// This is copy-paste from description.php!
$conn = new mysqli("localhost", "root", "toor", "webshop");
if ($conn->connect_error)
  die("Connection to database failed:" .
    $conn->connect_error);
 
$conn->query("set names utf8");
 
$statement = $conn->prepare(
"SELECT * FROM shop_user
WHERE email = ? AND password = PASSWORD(?)"); // I PROMISE I WILL NEVER USE THIS IN PRODUCTION
 
$statement->bind_param("ss", $_POST["user"], $_POST["password"]);
$statement->execute();
$results = $statement->get_result();
$row = $results->fetch_assoc();
$previous = strtok($_SERVER["HTTP_REFERER"],'?');

if($row) { 
    $_SESSION["user"] = $row["id"]; // This just stores user row number!
		$_SESSION["logged_in"] = "loginsuccess";
		header("Location: $previous");
} else {
		$_SESSION["logged_in"] = "loginfailed";
		header("Location: $previous");
}
?>
