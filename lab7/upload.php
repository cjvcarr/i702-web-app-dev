<html>
	<?php include ('header.php'); ?>
  <body>
    <form method="post" enctype="multipart/form-data">
      <input type="text" name="product_title"/>
      <input type="text" name="product_description"/>
      <input type="file" name="product_image" required/>
      <input type="file" name="product_thumbnail"/>
      <input type="submit"/>
    </form>
    <!-- You also need:
    mkdir uploads
    chmod 777
    -->
    <?php
    if (array_key_exists("product_image", $_FILES)) {
      $finfo = finfo_open(FILEINFO_MIME_TYPE);
      $mimetype = finfo_file($finfo, $_FILES["product_image"]["tmp_name"]);
      if ($mimetype != "image/jpeg" && $mimetype != "image/png") die("Go away!");
 
      $checksum = sha1(file_get_contents(
        $_FILES["product_image"]["tmp_name"])) . "." .
        pathinfo($_FILES["product_image"]["name"], PATHINFO_EXTENSION);
      if (!file_exists("uploads/" . $checksum)) {
        copy(
          $_FILES["product_image"]["tmp_name"],
          "uploads/" . $checksum);
      }
    }
    ?>
    <p>Mimetype was: <?= $mimetype; ?></p>
    <p>Checksum was: <a href="uploads/<?=$checksum;?>"><?=$checksum;?></a>
    <p>Filename was: <?=$_FILES["product_image"]["name"];?></p>
    <p>File stored at: <?=$_FILES["product_image"]["tmp_name"];?></p>
  </body>
</html>
