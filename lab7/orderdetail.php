<a href="orders.php">Back to order list</a>
 
<?php
include("header.php");
$conn = new mysqli("localhost", "root", "toor", "webshop");
if ($conn->connect_error)
  die("Connection to database failed:" .
    $conn->connect_error);
$statement = $conn->prepare('SELECT `product_id`, `count`, `unit_price` FROM `shop_order_item` WHERE `order_id` = ?');
$statement->bind_param("i", $_GET["id"]);
$statement->execute();
$results = $statement->get_result();

?>
<div class="table-responsive">
<table class="table table-striped table-hover">
<thead>
  <tr>
    <th>Product name</th>
	<th>Unit Price</th>
    <th>QTY</th> 
	<th>Total Price</th>
  </tr>
</thead>
<tbody>
<?php
$grand_total = 0;
while ($row = $results->fetch_assoc()) {
$statement_product = $conn->prepare('SELECT `name` FROM `shop_products` WHERE `id` = ?');
$statement_product->bind_param("i", $row['product_id']);
$statement_product->execute();
$product_res = $statement_product->get_result();
$products = $product_res->fetch_assoc();
$total = floatval($row['count'])*floatval($row['unit_price']);
?>
  <tr>
    <td><?=$products['name'];?></td>
	<td><?=$row['unit_price'].'€';?></td>
    <td><?=$row['count'];?></td> 
	<td><?=$total.'€';?></td>
  </tr>
<?php 
  $grand_total = $grand_total + $total;
} ?>
</tbody>
<tfoot>
  <tr>
	<td>Grand total</td>
	<td></td>
	<td></td>
	<td><?=$grand_total.'€';?></td>
</tfoot>
</table>
</div>
